$(document).ready(function(){
    var container = $('#feedContainer');
    var ul = $('#list');
    var h1 = $('h1');
    var infos;
    var feeds;
    var current_video;
    
    h1.animate({
        left: '39.6%'
    }, 2000);
    
    function initialize() {
        
        var feed = new google.feeds.Feed("https://feeds.feedburner.com/tedtalks_video");
        feed.setNumEntries(100);
        feed.load(function(result){
           if (!result.error) {
                for(var i = 0; i < result.feed.entries.length; i++){
                    var entry = result.feed.entries[i];
                    var infoli = $('<li class="info"></li>');
                    var li = $('<li class="feed"></li>');
                    var h5 = $('<h5 class="feed"></h5>');
                    var video = $('<video controls></video>');
                    var p_published = $('<p class="pdate"></p>');
                    var p_content = $('<p></p>');
                    var button = $('<button class="closebtn">&#10006;</button>');
                    button.on('click', closeFunc);
                    h5.text(entry.title+'...');
                    h5.appendTo(li);
                    li.appendTo(ul);
                    button.appendTo(infoli);
                    
                    video.css({
                       width: '500',
                       height: '300'    
                    });
                    
                    infoli.data('videosrc', entry.mediaGroups[0].contents[0].url);
                    video.attr('src', '');
                    video.appendTo(infoli);
                    p_published.text('Posted: '+entry.publishedDate.slice(0, 17));
                    p_published.appendTo(infoli);
                    p_content.text(entry.content.slice(0, entry.content.indexOf('<')));
                    p_content.appendTo(infoli);
                    infoli.appendTo(ul);
                }
                infos = $('.info');
                feeds = $('.feed');
            }
        });
    }
    
    google.setOnLoadCallback(initialize);
    
    ul.on('click', function(e){
        var elem = $(e.target);
        var nextElem;
        var siblings;
        var self;
        if(elem.hasClass('feed')){
            if(elem.is('li')){
                nextElem = elem.next();
                siblings = elem.siblings();
                self = elem;
            }else{
                var parent = elem.parent();
                nextElem = parent.next();
                siblings = parent.siblings();
                self = parent;
            }
            
            nextElem.css({
               width: '40%',
               marginLeft: '27%'
            });
            
            current_video = nextElem.find('video').get(0);
            var clsbtn = nextElem.find('.closebtn');
            var videolink = nextElem.data('videosrc');
            $(current_video).attr('src', videolink);
            siblings.hide(1000, function(){
                self.hide(1000, function(){
                    clsbtn.show();
                    nextElem.show(900);
                    current_video.play();
                });
            });
        }
    });
    
    function closeFunc(){
        $(this).hide();
        if(!current_video.paused){
            current_video.pause();
            current_video.currentTime = 0;
            $(current_video).attr('src', '');
        }
        infos.hide(10);
        feeds.show(1000);
    }
});